# PhamLuan Database

***Nội dung***
- tìm hiểu database là gì
- Sơ đồ ER và cách vẽ sơ đồi ER
- cách tạo database trên mysql server, cách tạo các bảng, kiểu dữ liệu
- truy vấn và tối ưu truy vấn sql

- [BaiTapER](https://docs.google.com/document/d/1QtbX-ld_kF3GVb4xCYnXv6w5qFaZ4zgRCzmW75tn67c/edit)
- [BaitapCSDL](https://docs.google.com/document/d/16fkpdzAPnkSLoSoUVLS4iSydvn9xCTzD/edit)
- [BaitapSQL](https://docs.google.com/document/d/12_YVcz4yvlOZ2fImxwjA2QGq8nRUl1Io/edit)

thực hiện bởi [Phạm Luận](https://facebook.com/phluann)

## Kiến thức nắm được   
***Database là gì?***
- là một tập hợp các dữ liệu:
    - biểu diễn một khía cạnh nào đó của thế giới thực
    - có liên hệ logic thống nhất
    - được thiết kế và bao gồm những dữ liệu phục vụ một mục đích nào đó
- là một bộ sưu tập các dữ liệu tác nghiệp được lưu trữ lại và được các hệ ứng dụng cụ thể nào đó sử dụng

***Mô hình thực thể liên kết (ER)***
- cho phép mô tả các dữ liệu có liên quan trong một xí nghiệp trong thế giới thực dưới dạng các đối tượng và các mối quan hệ của chúng
- được sử dụng cho bước đầu thiết kế CSDL, làm nền tảng để ánh xạ sang một mô hình khái niệm nào đó mà hệ quản trị CSDL sẽ sử dụng
- các khái niệm cơ bản:
    - thực thể: là một đối tượng trong thế giới thực, tồn tại độc lập và phân biệt được với các đối tượng khác (mô tả bằng hình chữ nhật)
    - tập thực thể: một tập hợp các thực thể có tính chất giống nhau 
    - thuộc tính: là đặc tính của một tập thực thể (mô tả bằng hình elipse)
        - thuộc tính đơn giản
        - thuộc tính phức: thuộc tính được định nghĩa bởi các thuộc tính khác 
        - thuộc tính đa trị: tương ứng với mỗi thực thể có thể có nhiều giá trị (mô tả bằng 2 hình elipse lồng nhau)
        - thuộc tính suy diễn: có thể tính toán được từ các thuộc tính khác (mô tả bằng hình elipse nét đứt)
    - khóa: một hay một tập thuộc tính mà giá trị của chúng có thể xác định duy nhất một thực thể trong tập thực thể (thuộc tính khóa chính được gạch chân)
    - liên kết: một liên kết là một mối liên hệ có nghĩa giữa nhiều thực thể (mô tả bằng hình thoi)
    - tập liên kết là một tập hợp các liên kết cùng kiểu
    - ràng buộc của kết nối: 1-1, 1-n, n-n, đệ quy

***Cách vẽ sơ đồ ER***
- bước 1: xác định các thực thể và các thuộc tính của chúng
- bước 2: xác định liên kết và ràng buộc số lượng (1-1, 1-n, n-n, đệ quy) giữa các thực thể

***Tạo database, tạo bảng bằng câu lệnh SQL***
- tạo database: `CREATE DATABASE name;`
- tạo bảng:
    ```
    CREATE TABLE table_name (
        cột-1 kiểu-dữ-liệu-1,
        cột-2 kiểu-dữ-liệu-2,
        ...,
        PRIMARY KEY (tên-cột),
        FOREIGN KEY (tên-cột) REFERENCES tên-bảng (tên-cột)
    );

    ví dụ:
    CREATE TABLE product(
        pid int(10) NOT NULL, 
        pname varchar(50) NOT NULL,
        price float NOT NULL,
        pcolor varchar(255),
        PRIMARY KEY(pid)
    );

    CREATE TABLE invoice(
        id int(10) PRIMARY KEY NOT NULL,
        pid int(10) NOT NULL,
        FOREIGN KEY(pid) REFERENCES product(pid)
    );
    ```

***Kiểu dữ liệu***
- kiểu số: bit, tinyint, smallint, int, bigint, demical, numeric, float, float, real
- kiểu ngày giờ: DATE, TIME, YEAR, DATETIME, TIMESTAMP
- kiểu chữ: char, varchar, text

***Try vấn SQL***

- select:   
`SELECT <ds cột> FROM <ds bảng> [WHERE <Điều kiện>]`

    ví dụ: lấy ra toàn bản ghi trong bảng product:

        SELECT * FROM product;      

    lấy ra pid từ bảng product và id từ bảng invoice, product name nếu productID = pid trong bảng invoice:

        SELECT 
        product.pid as pid, invoice.id as iid , product.pname as name 
        FROM product, invoice  
        WHERE product.pid = invoice.pid;

- update:

    `UPDATE <tên bảng> SET <tên cột> = <giá trị> WHERE <điều kiện>`

    Ví dụ: Sửa tên của sản phẩm có id = 1 thành 'tong laos' của bảng product

        UPDATE product SET pname = 'tong laos' WHERE pid = 1;

- delete:

    `DELETE FROM <tên bảng> WHERE <điều kiện>`

    Ví dụ: xóa sản phẩm có tên = 'tong laos' trong bảng product

        DELETE FROM product WHERE pname = 'tong laos';

***Join và các loại join***
- JOIN (hay INNER JOIN): trả về tất cả các hàng khi có ít nhất một giá trị ở cả 2 bảng (điều kiện thỏa mãn ở cả 2 bảng)
    ```
        SELECT <ds cột> FROM <tên bảng 1> JOIN <tên bảng 2>
        ON <điều kiện>
    ```
    Ví dụ:
    ```
        SELECT * FROM product JOIN invoice
        ON product.pid = invoice.pid;
    ```
- LEFT JOIN (hay LEFT OUTER JOIN): trả về tất cả các bản ghi từ bảng bên trái với các hàng tương ứng trong bảng bên phải
    ```
        SELECT <ds cột> FROM <tên bảng 1> LEFT JOIN <tên bảng 2> 
        ON <điều kiện>
    ```
    Ví dụ:
    ```
        SELECT * FROM product LEFT JOIN invoice
        ON product.pid = invoice.pid;
    ```

- RIGHT JOIN (hay RIGHT OUTER JOIN): trả về tất cả các bản ghi từ bảng bên phải với các bản ghi tương ứng ở bảng bên trái
    ```
        SELECT <ds cột> FROM <tên bảng 1> RIGHT JOIN <tên bảng 2> 
        ON <điều kiện>
    ```
    Ví dụ: 
    ```
        SELECT * FROM product RIGHT JOIN invoice
        ON product.pid = invoice.pid;
    ```
- FULL JOIN: lấy ra tất cả các bản ghi của bảng bên trái và bên phải (thỏa mãn 1 trong 2 bảng)
    ```
        SELECT <ds cột>
        FROM <tên bảng 1> FULL JOIN <tên bảng 2>
    ```
    Ví dụ:
    ```
        SELECT *
        FROM product FULL JOIN invoice        
    ```
- SELFT JOIN: để join một bảng với chính nó
    ```
        SELECT <ds cột>
        FROM bang1 tbl1, bang1 tbl2
        WHERE tbl1.cot1 = tbl2.cot2
    ```

    Ví dụ:
    ```
        SELECT *
        FROM product a, product b
        WHERE a.pname = b.pcolor;
    ```
